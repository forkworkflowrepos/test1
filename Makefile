CC = gcc
IPATH = ./include
CFLAGS = -I$(IPATH)

main: main.o
	$(CC) $(CFLAGS) main.o -o main

main.o: main.c
	$(CC) -c -o main.o $(CFLAGS)
